

from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication


class Form1:
    def __init__(self):
        self.mainfrm1 = loadUi('form1.ui')
        
class Form2:
    def __init__(self):
        self.mainfrm2 = loadUi('form2.ui')

class Form3:
    def __init__(self):
        self.mainfrm3 = loadUi('form3.ui')
        
class ChkFormShow:
    def __init__(self):
        self.mainui = loadUi('chkshowform.ui')
        self.mainui.show()
        self.mainui.move(300, 400)
        
        self.mainui.chkForm1.stateChanged.connect(self.form1)
        self.mainui.chkForm2.stateChanged.connect(self.form2)
        self.mainui.chkForm3.stateChanged.connect(self.form3)

    def form1(self):
        if self.mainui.chkForm1.isChecked():
            self.extform1 = Form1()
            self.extform1.mainfrm1.show()
            self.extform1.mainfrm1.move(50, 100)
        else:
            self.extform1.mainfrm1.close()
            
    def form2(self):
        if self.mainui.chkForm2.isChecked():
            self.extform2 = Form2()
            self.extform2.mainfrm2.show()
            self.extform2.mainfrm2.move(380, 100)
        else:
            self.extform2.mainfrm2.close()
            
    def form3(self):
        if self.mainui.chkForm3.isChecked():
            self.extform3 = Form3()
            self.extform3.mainfrm3.show()
            self.extform3.mainfrm3.move(700, 100)
        else:
            self.extform3.mainfrm3.close()

if __name__ == '__main__':
    app = QApplication([])
    main = ChkFormShow()
    app.exec()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    